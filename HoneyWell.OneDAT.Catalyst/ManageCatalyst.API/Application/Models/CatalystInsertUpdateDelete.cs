﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageCatalyst.API.Application.Models
{
    public class ApplicationUserSearch
    {
        public string EMPLOYEE_ID { get; set; }

        public string FIRST_NM { get; set; }
        public string LAST_NM { get; set; }
    }
    public class LimsSample
    {
        public string LIMS_SAMPLE_ID { get; set; }
    }
    public class CatalystId
    {
        public int CATALYST_ID_SQ { get; set; }
    }
    public class CatalystSearch
    {
        public int? catalyst_id_sq { get; set; }
        public string catalyst_designation_nm { get; set; }
        public string catalyst_desc { get; set; }
        public int? catalyst_family_id { get; set; }
        public string family_name { get; set; }
        public int? catalyst_type_id { get; set; }
        public string catalyst_type_nm { get; set; }
        public int? catalyst_shape_id { get; set; }
        public string catalyst_shape_nm { get; set; }
        public int? catalyst_size_id { get; set; }
        public string catalyst_size_cd { get; set; }
        public int? catalyst_alias_ss_id { get; set; }
        public string reference_catalyst_ind { get; set; }
        public string regenerated_catalyst_ind { get; set; }
        public int? catalyst_state_id { get; set; }
        public string catalyst_state_nm { get; set; }
        public string ground_ind { get; set; }
        public string analytical_approve_ind { get; set; }
        public string analytical_status { get; set; }
        public string bulk_location_ind { get; set; }
        public string catalyst_leader { get; set; }
        public string catalyst_leader_nm { get; set; }
        public decimal? apparent_bed_density_msr { get; set; }
        public decimal? vibrated_bed_density_msr { get; set; }
        public int? catalyst_scale_id { get; set; }
        public decimal? piece_density_msr { get; set; }
        public decimal? loi_500_msr { get; set; }
        public decimal? assumed_fraction_msr { get; set; }
        public decimal? crushed_fraction_msr { get; set; }
        public char active_ind { get; set; }
    }
    public class RunCatalystDelete
    {
        public int countCatalyst { get; set; }
    }
}
