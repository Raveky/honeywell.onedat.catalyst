﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using HoneyWell.OneDAT.Commons.Integration.Interfaces;
using ManageCatalyst.API.Application.Models;
using Newtonsoft.Json;
using HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Validators;
namespace HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Services
{
    public class ManageCatalystService : OneDATService
    {
        Operations operations;
        private readonly ISender sender;
        private readonly IReceiver receiver;
        Query query;
        BusinessValidator bussinessValidator;
        OperationStatus objResult;

        public ManageCatalystService(IConfiguration configuration, ISender _sender, IReceiver _receiver) : base(configuration)
        {
            operations = new Operations(configuration);
            sender = _sender;
            receiver = _receiver;
            query = new Query();
            bussinessValidator = new BusinessValidator(configuration);
            objResult = new OperationStatus();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            List<LimsSample> sampleIdList = new List<LimsSample>();
            OperationStatus operationStatus = new OperationStatus();
            Field sample = new Field();
            string created_by_user_id = null;
            int catalyst_id_sq = 0;
            List<CatalystId> catalyst = new List<CatalystId>();
            foreach (var element in dataRequest.Messages[0].fields)
            {
                switch (element.field_name.ToUpper())
                {
                    case "SAMPLE_LIST_ID":
                        sampleIdList.AddRange(JsonConvert.DeserializeObject<List<LimsSample>>(element.field_value));
                        sample = element;
                        break;
                    case "CREATED_BY_USER_ID":
                        created_by_user_id = element.field_value;
                        break;
                }
            }
            //Removing SAMPLE_LIST_ID field from dataRequest object
            dataRequest.Messages[0].fields.Remove(sample);
            //Fetching Primarky Key Value
            operationStatus = await operations.Read(dataRequest, query.GetQuery());
            catalyst = JsonConvert.DeserializeObject<List<CatalystId>>(JsonConvert.SerializeObject(operationStatus.DataResponse[0].Data));
            catalyst_id_sq = catalyst[0].CATALYST_ID_SQ + 1;
            //Adding primay key filed to dataRequest Object
            dataRequest.Messages[0].fields.Add(new Field
            {
                field_name = "CATALYST_ID_SQ",
                field_type = "int",
                field_value = catalyst_id_sq.ToString()
            });
            // Inserting record
            var response = await operations.Create(dataRequest);
            if (response.Status == "Fail")
                return response;
            // Executing query which having delete, insert or update operations
            string sqlQuery = query.GetAddAndUpdateQuery(sampleIdList, catalyst_id_sq, created_by_user_id);
            var subResponse = await operations.ExecuteScalarQuery(sqlQuery);
            if (subResponse == "Fail")
            {
                objResult.Status = "Fail";
                objResult.Message = "Error occured inside detele query";
                return objResult;
            }
            // Creating a DataRequest, adding a catalyst_id_sq field as filter
            DataRequest data = new DataRequest();
            List<Message> messages = new List<Message>();
            List<Field> filter = new List<Field>();
            filter.Add(new Field
            {
                field_name = "catalyst_id_sq",
                field_type = "int",
                field_value = catalyst_id_sq.ToString()
            });
            messages.Add(new Message
            {
                template_name = string.Empty,
                table_name = "sc_Catalyst.CATALYST",
                fields = null,
                filter = filter
            });

            data.OperationType = "GET";
            data.Messages = messages;
            // calling other method which having update / insert operations
            return operationStatus;
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here
            OperationStatus operationStatus = new OperationStatus();
            string sqlQuery = query.GetSearchQuery(dataRequest);
            dataRequest.Messages[0].filter = null;
            //Getting Data from USER workerservice
            dataRequest.Messages[0].table_name = "APPLICATIONUSEREMPLOYEEDATA";
            string UserSessionId = Guid.NewGuid().ToString();
            await sender.SendAsync<DataRequest>(dataRequest, UserSessionId, "UserDev");
            var UserResult = await receiver.ReceiveAsync<List<ApplicationUserSearch>>(UserSessionId, 2);
            //Getting Catalyst Data
            operationStatus = await operations.Read(dataRequest, sqlQuery);
            var catalystResult = JsonConvert.DeserializeObject<List<CatalystSearch>>(JsonConvert.SerializeObject(operationStatus.DataResponse[0].Data));

            var finalResult = (from catalyst in catalystResult
                               join user in UserResult on catalyst.catalyst_leader equals user.EMPLOYEE_ID
                               select new
                               {
                                   catalyst.catalyst_id_sq,
                                   catalyst.catalyst_designation_nm,
                                   catalyst.catalyst_desc,
                                   catalyst.catalyst_family_id,
                                   catalyst.family_name,
                                   catalyst.catalyst_type_id,
                                   catalyst.catalyst_type_nm,
                                   catalyst.catalyst_shape_id,
                                   catalyst.catalyst_shape_nm,
                                   catalyst.catalyst_size_id,
                                   catalyst.catalyst_size_cd,
                                   catalyst.catalyst_alias_ss_id,
                                   reference_catalyst_ind = (catalyst.reference_catalyst_ind == "Y") ? true : false,
                                   regenerated_catalyst_ind = (catalyst.regenerated_catalyst_ind == "Y") ? true : false,
                                   catalyst.catalyst_state_id,
                                   catalyst.catalyst_state_nm,
                                   ground_ind = (catalyst.ground_ind == "Y") ? true : false,
                                   analytical_approve_ind = (catalyst.analytical_approve_ind == "Y") ? true : false,
                                   catalyst.analytical_status,
                                   bulk_location_ind = (catalyst.bulk_location_ind == "Y") ? true : false,
                                   catalyst.catalyst_leader,
                                   catalyst_leader_nm = user.LAST_NM + ", " + user.FIRST_NM,
                                   catalyst.apparent_bed_density_msr,
                                   catalyst.vibrated_bed_density_msr,
                                   catalyst.catalyst_scale_id,
                                   catalyst.piece_density_msr,
                                   catalyst.loi_500_msr,
                                   catalyst.assumed_fraction_msr,
                                   catalyst.crushed_fraction_msr,
                                   catalyst.active_ind
                               }).ToList<dynamic>();

            List<DataResponse> dataResponse = new List<DataResponse> { };
            dataResponse.Add(new DataResponse { Data = finalResult });

            return new OperationStatus { Status = "Success", DataResponse = dataResponse };
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            List<LimsSample> sampleIdList = new List<LimsSample>();
            OperationStatus operationStatus = new OperationStatus();
            Field sample = new Field();
            string updated_by_user_id = null;
            int? catalyst_id_sq = null;
            foreach (var element in dataRequest.Messages[0].fields)
            {
                switch (element.field_name.ToUpper())
                {
                    case "SAMPLE_LIST_ID":
                        sampleIdList.AddRange(JsonConvert.DeserializeObject<List<LimsSample>>(element.field_value));
                        sample = element;
                        break;
                    case "UPDATED_BY_USER_ID":
                        updated_by_user_id = element.field_value;
                        break;
                }
            }
            foreach (var element in dataRequest.Messages[0].filter)
            {
                switch (element.field_name.ToUpper())
                {
                    case "CATALYST_ID_SQ":
                        catalyst_id_sq = Convert.ToInt32(element.field_value);
                        break;
                }
            }
            //Removing SAMPLE_LIST_ID field from dataRequest object
            dataRequest.Messages[0].fields.Remove(sample);
            // Updating record 
            operationStatus = await operations.Update(dataRequest);
            if (operationStatus.Status == "Fail")
                return operationStatus;
            // Executing query which having delete, insert or update operations
            string sqlQuery = query.GetAddAndUpdateQuery(sampleIdList, catalyst_id_sq, updated_by_user_id);
            var subResponse = await operations.ExecuteScalarQuery(sqlQuery);
            if (subResponse == "Fail")
            {
                objResult.Status = "Fail";
                objResult.Message = "Error occured inside detele query";
                return objResult;
            }
            // calling other method which having update / insert operations
            return objResult;
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            OperationStatus success = new OperationStatus();
            var readResult = await bussinessValidator.Validate(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            //Getting Data from Run workerservice
            dataRequest.OperationType = "GET";
            dataRequest.Messages[0].table_name = "RUNCATALYSTDELETE";
            string UserSessionId = Guid.NewGuid().ToString();
            await sender.SendAsync<DataRequest>(dataRequest, UserSessionId, "RunDev");
            var runCatalystResult = await receiver.ReceiveAsync<List<RunCatalystDelete>>(UserSessionId, 2);
            dataRequest.OperationType = "DELETE";
            // checking weather sc_Run.RUN_CATALYST table has any records
            if (runCatalystResult[0].countCatalyst <= 0)
            {
                success = await operations.ExecuteDbQuery(query.GetDeleteQuery(dataRequest));
                if (success.Status == "Success")
                    success.Message = "Record Deleted successfully";
            }
            else
            {
                success.Status = "Fail";
                success.Message = "Catalyst is used in sc_Run.RUN_CATALYST table";
            }

            return success;


        }

    }

}
