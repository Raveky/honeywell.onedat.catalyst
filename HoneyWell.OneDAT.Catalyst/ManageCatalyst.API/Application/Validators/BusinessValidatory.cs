﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Validators
{
    public class BusinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BusinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }

        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            bool catalyst_id = false;
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        if (data.field_name.ToLower() == "catalyst_id_sq")
                            if (string.IsNullOrEmpty(data.field_value))
                                return (false, "catalyst_id_sq value should not be null or empty");
                            else
                                catalyst_id = true;
                    }
                }
            }
            if (!catalyst_id)
                return (false, "catalyst_id_sq paramerter is manadatory to delete records");
            return (true, "valid Data");
        }
    }
}
