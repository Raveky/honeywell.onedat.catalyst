﻿using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using ManageCatalyst.API.Application.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;
        public string GetQuery(string QueryName)
        {
            throw new NotImplementedException();
        }

        public string GetQuery()
        {
            _Query = "SELECT MAX(CATALYST_ID_SQ) CATALYST_ID_SQ FROM sc_Catalyst.Catalyst";
            return _Query;
        }

        public string GetSearchQuery(DataRequest dataRequest)
        {
            string catalyst_designation_nm_search = null;
            string catalyst_desc_search = null;
            string catalyst_family_id_search = "null";//int
            // int? CatalystTypeId=null
            string catalyst_shape_id_search = "null"; //int
            string catalyst_size_id_search = "null"; //int
            string catalyst_alias_ss_id_search = "null"; //int
            // string ReferenceCatalystInd = null;
            //string RegenCatalystInd=null;
            string catalyst_state_id_search = "null"; //int
            //string GroundInd = null;
            //string AnalyticalApproveInd = null;
            // string analystical_status_search = null;
            //string BulkLocationInd = null;
            string catalyst_leader_search = null;
            //string  apparent_bed_density_msr_search = null;
            string vibrated_bed_density_msr_search = "null"; // need to check
            string catalyst_scale_id_search = "null";  //int
            //string CatalystPieceDensity = null;
            //string LOIAt500Msr=null;
            //string CatalystVoidFraction=null;
            //string active_ind_search = null;
            string isInitialLoad = null;

            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var ele in dataRequest.Messages[0].filter)
                    {
                        switch (ele.field_name.ToLower())
                        {
                            case "catalyst_designation_nm":
                                catalyst_designation_nm_search = ele.field_value;
                                break;
                            case "catalyst_desc":
                                catalyst_desc_search = ele.field_value;
                                break;
                            case "catalyst_family_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_family_id_search = ele.field_value;
                                break;
                            case "catalyst_shape_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_shape_id_search = ele.field_value;
                                break;
                            case "catalyst_size_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_size_id_search = ele.field_value;
                                break;
                            case "catalyst_alias_ss_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_alias_ss_id_search = ele.field_value;
                                break;
                            case "catalyst_state_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_state_id_search = ele.field_value;
                                break;
                            case "catalyst_leader":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_leader_search = ele.field_value;
                                break;
                            case "vibrated_bed_density_msr":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    vibrated_bed_density_msr_search = ele.field_value;
                                break;
                            case "catalyst_scale_id":
                                if (!string.IsNullOrEmpty(ele.field_value))
                                    catalyst_scale_id_search = ele.field_value;
                                break;
                            case "isinitialload":
                                isInitialLoad = ele.field_value;
                                break;
                        }
                    }
                }
            }

            _Query = " SELECT " +
                                    "[CATALYST_ID_SQ]," +
                                   "[CATALYST_DESIGNATION_NM]" +
                                   ",[CATALYST_DESC]" +
                                   ",[CATALYST_FAMILY_ID]" +
                                   ",CASE WHEN cf.CATALYST_FAMILY_IND='C' THEN COMMERCIAL_FAMILY_NM ELSE PROGRAM_FAMILY_NM END AS FAMILY_NAME" +
                                   ",ct.[CATALYST_TYPE_ID_SQ] as [CATALYST_TYPE_ID]" +
                                   ",ct.CATALYST_TYPE_NM" +
                                   ",[CATALYST_SHAPE_ID]" +
                                   ",csh.CATALYST_SHAPE_NM" +
                                   ",[CATALYST_SIZE_ID]" +
                                   ",cs.CATALYST_SIZE_CD" +
                                   ",[CATALYST_ALIAS_SS_ID]" +
                                   ",[REFERENCE_CATALYST_IND]" +
                                   ",[REGENERATED_CATALYST_IND]" +
                                   ",[CATALYST_STATE_ID]" +
                                   ",cst.CATALYST_STATE_NM" +
                                   ",[GROUND_IND]" +
                                   ",[ANALYTICAL_APPROVE_IND]" +
                                   ",[ANALYTICAL_STATUS]" +
                                   ",[BULK_LOCATION_IND]" +
                                   ",[CATALYST_LEADER]" +
                                   ",[APPARENT_BED_DENSITY_MSR]" +
                                   ",[VIBRATED_BED_DENSITY_MSR]" +
                                   ",c.[CATALYST_SCALE_ID]" +
                                   ",[PIECE_DENSITY_MSR]" +
                                   ",[LOI_500_MSR]" +
                                   ",csh.ASSUMED_VOIDFRACTION_MSR as ASSUMED_FRACTION_MSR" +
                                   ",csh.CRUSHED_FRACTION_MSR as CRUSHED_FRACTION_MSR " +
                                   ",c.[ACTIVE_IND] " +
             "FROM [sc_Catalyst].[CATALYST] C LEFT JOIN [sc_Catalyst].[CATALYST_FAMILY] cf on cf.CATALYST_FAMILY_ID_SQ=c.CATALYST_FAMILY_ID" +
                    " LEFT JOIN [sc_Catalyst].[CATALYST_TYPE] ct on ct.CATALYST_TYPE_ID_SQ=c.CATALYST_TYPE_ID" +
                    " LEFT JOIN [sc_Catalyst].[CATALYST_SIZE] cs on cs.CATALYST_SIZE_ID_SQ=c.CATALYST_SIZE_ID" +
                    " LEFT JOIN [sc_Catalyst].[CATALYST_SHAPE] csh on csh.CATALYST_SHAPE_ID_SQ=c.CATALYST_SHAPE_ID" +
                    " LEFT JOIN [sc_Catalyst].[CATALYST_STATE] cst on cst.CATALYST_STATE_ID_SQ=c.CATALYST_STATE_ID" +
                    " WHERE" +
                    "(('" + catalyst_designation_nm_search + "' IS NULL OR '" + catalyst_designation_nm_search + "' ='' OR '" + isInitialLoad + "'='Y') OR C.CATALYST_DESIGNATION_NM like '%' + '" + catalyst_designation_nm_search + "' + '%') AND" +
                    " (('" + catalyst_desc_search + "' IS NULL OR '" + catalyst_desc_search + "'='' OR '" + isInitialLoad + "'='Y') OR C.CATALYST_DESC like '%'+ '" + catalyst_desc_search + "' + '%') AND" +
                    " ((" + catalyst_family_id_search + " IS NULL OR " + catalyst_family_id_search + "= 0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_FAMILY_ID= " + catalyst_family_id_search + ") AND" +
                    " ((" + catalyst_shape_id_search + " IS NULL OR " + catalyst_shape_id_search + " =0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_SHAPE_ID=" + catalyst_shape_id_search + ") AND" +
                    " ((" + catalyst_size_id_search + " IS NULL OR " + catalyst_size_id_search + "=0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_SIZE_ID=" + catalyst_size_id_search + ") AND" +
                    " ((" + catalyst_alias_ss_id_search + " IS NULL OR " + catalyst_alias_ss_id_search + "=0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_ALIAS_SS_ID=" + catalyst_alias_ss_id_search + ") AND" +
                    " ((" + catalyst_scale_id_search + " IS NULL OR " + catalyst_scale_id_search + " =0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_SCALE_ID=" + catalyst_scale_id_search + ") AND" +
                    " ((" + catalyst_state_id_search + " IS NULL OR " + catalyst_state_id_search + "=0 OR '" + isInitialLoad + "'='Y') OR C.CATALYST_STATE_ID=" + catalyst_state_id_search + ") AND" +
                    //--((@AnalyticalStatus IS NULL OR @AnalyticalStatus='' OR @IsInitialLoad='Y')OR C.ANALYTICAL_STATUS=@AnalyticalStatus) AND  		--not  searchable 
                    " (('" + catalyst_leader_search + "' IS NULL OR '" + catalyst_leader_search + "'='' OR '" + isInitialLoad + "'='Y') OR C.CATALYST_LEADER='" + catalyst_leader_search + "') AND" +
                    //--((@ApparentBedDensity IS NULL OR @IsInitialLoad='Y')OR C.APPARENT_BED_DENSITY_MSR=@ApparentBedDensity) AND"
                    " ((" + vibrated_bed_density_msr_search + " IS NULL OR '" + isInitialLoad + "' ='Y') OR C.VIBRATED_BED_DENSITY_MSR= " + vibrated_bed_density_msr_search + ")";

            return _Query;
        }

        public string GetDeleteQuery(DataRequest dataRequest)
        {
            string catalyst_Id = "";
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        if (data.field_name.ToLower() == "catalyst_id_sq")
                            if (!string.IsNullOrEmpty(data.field_value))
                                catalyst_Id = data.field_value;
                    }
                }
            }
            _Query = "BEGIN TRANSACTION ";
            // Delete statemtn for [CATALYST_PROPERTY_VALUE] table
            _Query += "  DELETE FROM [sc_Catalyst].[CATALYST_PROPERTY_VALUE] WHERE CATALYST_PROPERTY_ID IN(SELECT CATALYST_PROPERTY_ID_SQ FROM sc_Catalyst.CATALYST_PROPERTY WHERE CATALYST_ID_SQ= '" + catalyst_Id + "');";
            // Delete statemtn for [CATALYST_PROPERTY] table
            _Query += "  DELETE FROM [sc_Catalyst].[CATALYST_PROPERTY] WHERE CATALYST_ID_SQ ='" + catalyst_Id + "';";
            // Delete statemtn for [CATALYST_LIMS_SAMPLE_ASSOC] table
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST_LIMS_SAMPLE_INFO] WHERE CATALYST_ID ='" + catalyst_Id + "';";
            // Delete statemtn for [CATALYST] table
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST] WHERE CATALYST_ID_SQ ='" + catalyst_Id + "';";
            _Query += " COMMIT TRANSACTION";

            return _Query;
        }

        public string GetAddAndUpdateQuery(List<LimsSample> limsSamplesLIst, int? catalyst_id_sq, string user_id)
        {
            string sample = "";
            if (limsSamplesLIst.Count > 0)
            {
                // creating a values set insetead of Temporary table
                sample += "(VALUES";
                for (int i = 0; i < limsSamplesLIst.Count; i++)
                {
                    if (i < limsSamplesLIst.Count - 1)
                        sample += "(" + catalyst_id_sq + ",'" + limsSamplesLIst[i].LIMS_SAMPLE_ID + "'),";
                    else
                        sample += "(" + catalyst_id_sq + ",'" + limsSamplesLIst[i].LIMS_SAMPLE_ID + "')) AS tbl(CatalystId,SampleId)";
                }
            }
            else
                sample += "(VALUES('','')) AS tbl(CatalystId, SampleId)";
            if (limsSamplesLIst.Count > 0)
            {
                _Query = " DELETE FROM sc_Catalyst.CATALYST_PROPERTY_VALUE WHERE CATALYST_PROPERTY_ID IN (SELECT CATALYST_PROPERTY_ID FROM sc_Catalyst.CATALYST_PROPERTY WHERE CATALYST_ID_SQ= " + catalyst_id_sq + " and LIMS_SAMPLE_ID IN (SELECT LIMS_SAMPLE_ID FROM sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO CLA WHERE CLA.CATALYST_ID= " + catalyst_id_sq + " and cla.LIMS_SAMPLE_ID NOT IN (select sampleId from " + sample + "))); ";
                _Query += " DELETE FROM sc_Catalyst.CATALYST_PROPERTY WHERE CATALYST_ID_SQ= " + catalyst_id_sq + " and LIMS_SAMPLE_ID IN (SELECT LIMS_SAMPLE_ID FROM sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO CLA WHERE CLA.CATALYST_ID= " + catalyst_id_sq + " and cla.LIMS_SAMPLE_ID NOT IN (select sampleId from " + sample + "));";

                _Query += " DELETE FROM sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO WHERE LIMS_SAMPLE_ID in (" +
                         "SELECT LIMS_SAMPLE_ID FROM sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO CLA WHERE CLA.CATALYST_ID= " + catalyst_id_sq + " and cla.LIMS_SAMPLE_ID NOT IN (select sampleId from " + sample + ")) and CATALYST_ID=" + catalyst_id_sq + ";";
            }
            else
            {
                _Query += " DELETE FROM sc_Catalyst.CATALYST_PROPERTY_VALUE WHERE CATALYST_PROPERTY_ID IN" +
                          " (SELECT CATALYST_PROPERTY_ID FROM sc_Catalyst.CATALYST_PROPERTY WHERE CATALYST_ID_SQ= " + catalyst_id_sq + ");";

                _Query += " DELETE FROM sc_Catalyst.CATALYST_PROPERTY WHERE CATALYST_ID_SQ = " + catalyst_id_sq + " ;";

                _Query += " DELETE FROM sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO WHERE CATALYST_ID = " + catalyst_id_sq + ";";
            }

            _Query += " MERGE into sc_Catalyst.CATALYST_LIMS_SAMPLE_INFO AS TARGET" +
                     " USING(SELECT t.SampleId, t.CatalystId" +
                     " FROM (SELECT * FROM " + sample + ")t) AS SOURCE" +
                     " ON(TARGET.CATALYST_ID = SOURCE.CatalystId AND TARGET.LIMS_SAMPLE_ID = SOURCE.SampleId)" +
                     " WHEN MATCHED THEN" +
                          " UPDATE SET TARGET.LIMS_SAMPLE_ID = SOURCE.SampleId,TARGET.UPDATED_BY_USER_ID = '" + user_id + "',TARGET.UPDATED_ON_DT = GETDATE()" +                   
                     " WHEN NOT MATCHED BY TARGET THEN" +
                    " INSERT(CATALYST_ID, LIMS_SAMPLE_ID, LIMS_DATA_UPLOAD_IND, CREATED_BY_USER_ID, CREATED_ON_DT)" +
                    " VALUES(SOURCE.CatalystId, SOURCE.SampleId, 'N','" + user_id + "', GETDATE());";
            return _Query;
        }

    }
}
