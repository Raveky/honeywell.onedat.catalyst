using HealthChecks.UI.Client;
using HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Controllers;
using HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Infrastructure.Filters;
using HoneyWell.OneDAT.Commons.Integration;
using HoneyWell.OneDAT.Commons.Integration.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;


namespace HoneyWell.OneDAT.Catalyst.ManageCatalyst.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            }).AddApplicationPart(typeof(ManageCatalystController).Assembly)
              .AddNewtonsoftJson();

            services.AddSwaggerGen();
            services.AddApplicationInsights(Configuration);
            services.AddHealthChecks(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

           services.AddSingleton<ISender>(ServiceProvider => new Sender(
           connectionString: Configuration.GetValue<string>("ServiceBus:ConnectionString"),
           topicName: Configuration.GetValue<string>("ServiceBus:TargetQueueName")));

           services.AddSingleton<IReceiver>(ServiceProvider => new Receiver(
           connectionString: Configuration.GetValue<string>("ServiceBus:ConnectionString"),
           queueName: Configuration.GetValue<string>("ServiceBus:SourceQueueName")));
            services.AddApplicationInsightsTelemetry(Configuration["APPINSIGHTS_INSTRUMENTATIONKEY"]);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });
            });
            app.UseSwagger(c =>
            {
                c.SerializeAsV2 = true;
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Riffle Request API V1");
            });


        }
    }


    static class CustomExtensionsMethods
    {
        public static IServiceCollection AddApplicationInsights(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddApplicationInsightsTelemetry(configuration["APPINSIGHTS_INSTRUMENTATIONKEY"]);
            services.AddApplicationInsightsKubernetesEnricher();

            return services;
        }


        public static IServiceCollection AddHealthChecks(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddCheck("self", () => HealthCheckResult.Healthy());

            hcBuilder.AddNpgSql(
                    configuration["ConnectionString"],
                    name: "OneDatDB-check",
                    tags: new string[] { "onedatdb" });

            return services;
        }

    }

}
