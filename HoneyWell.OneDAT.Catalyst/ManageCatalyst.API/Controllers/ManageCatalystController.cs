﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Application.Services;
using HoneyWell.OneDAT.Commons.Integration.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.ManageCatalyst.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ManageCatalystController : ControllerBase
    {
        private readonly ILogger<ManageCatalystController> _logger;

        private readonly IConfiguration _config;
        private readonly ISender sender;
        private readonly IReceiver receiver;

        public ManageCatalystController(ILogger<ManageCatalystController> logger, IConfiguration config, ISender _sender, IReceiver _receiver)
        {
            _logger = logger;

            _config = config;
            sender = _sender;
            receiver = _receiver;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            ManageCatalystService _rrs = new ManageCatalystService(_config, sender, receiver);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}
