﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.ManageMsds.API.Application.Validators;
using HoneyWell.OneDAT.CatalystLoadingConfiguration.API.Application.Validators;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;

namespace HoneyWell.OneDAT.Catalyst.ManageMsds.API.Application.Services
{
    public class ManageMsdsService : OneDATService
    {
        Operations operations;
        BussinessValidator bussinessValidator;
        OperationStatus objResult;
        public ManageMsdsService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BussinessValidator(configuration);
            objResult = new OperationStatus();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //validate logic goes here
            //throw new ValidationException("Validation Failed");
            var insertResult= await bussinessValidator.Validate(dataRequest);
            if (!insertResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = insertResult.message.ToString();
                return objResult;
            }
            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here

            //Business Logic, filters are not applied then default values shoud go
            var readResult= await bussinessValidator.ValidateRead(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            return await operations.Read(dataRequest);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here
            var updateRsult= await bussinessValidator.ValidateUpdate(dataRequest);
            if (!updateRsult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = updateRsult.message.ToString();
                return objResult;
            }
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            var deleteResult= await bussinessValidator.ValidateDelete(dataRequest);
            if (!deleteResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = deleteResult.message.ToString();
                return objResult;
            }
            return await operations.Delete(dataRequest);
        }

    }

}
