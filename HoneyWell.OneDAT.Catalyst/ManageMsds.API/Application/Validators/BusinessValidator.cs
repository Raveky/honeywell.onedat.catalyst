﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystLoadingConfiguration.API.Application.Validators
{
    public class BussinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            bool msds_cd = true;
            bool msds_cat_ind = true;
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "msds_cd")
                    {
                        msds_cd = false;
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank MSDS Field");
                        }

                    }
                    if (data.field_name == "msds_cat_ind")
                    {
                        msds_cat_ind = false;
                        if (data.field_value == "--Select--")
                        {
                            return (false, "select Catalyst/Feed indicator");
                        }                       
                    }
                }
                if (!msds_cd)
                    return (false, "MSDS field is missing");
                if (!msds_cat_ind)
                    return (false, "Catalyst/Feed Indicator is missing");
            }
            return (true, "Validate Data");
        }
        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        // based on msds# record will be updated
                        if (data.field_name == "msds_cd")
                        {
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "Blank MSDS Field");
                            }

                        }
                        if (data.field_name == "msds_cat_ind")
                        {
                            if (data.field_name == "--Select--")
                            {
                                return (false, "select Catalyst/Feed indicator");
                            }
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "Blank Catalyst/Feed Indicator");
                            }

                        }
                        if (data.field_name == "catalyst_cat_des")
                        {
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "Blank Description");
                            }

                        }
                    }

                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateUpdate(DataRequest dataRequest)
        {

            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    // based on msds# record will be updated
                    if (data.field_name == "msds_cat_ind")
                    {
                        if (data.field_name == "--Select--")
                        {
                            return (false, "select Catalyst/Feed indicator");
                        }
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Catalyst/Feed Indicator");
                        }
                    }
                    if(data.field_name== "msds_cat_desc")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Description");
                        }
                    }                   

                }
            }

            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    // based on msds# record will be updated
                    if (data.field_name == "msds_cd")
                    {
                        if (dataRequest.Messages[0].filter[0].field_value.Length < 1)
                        {
                            return (false, "Blank MSDS Field");
                        }
                    }
                   
                }              
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateDelete(DataRequest dataRequest)
        {
            bool msds_parameter = false;
            if (dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].filter)
                {
                    // based on msds# record will be deleted
                    if (data.field_name == "msds_cd")
                    {
                        msds_parameter = true;
                        if (String.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank MSDS");

                        }
                    }

                }
                if (!msds_parameter)
                    return (false, "MSDS field is missing");
            }

            return (true, "Valid Data");
        }

    }
}

