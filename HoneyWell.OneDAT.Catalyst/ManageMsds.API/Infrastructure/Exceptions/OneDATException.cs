﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Module.SubModule.API.Infrastructure.Exceptions
{
    //OneDATException

    public class OneDATException : Exception
    {
        public OneDATException()
        { }

        public OneDATException(string message)
            : base(message)
        { }

        public OneDATException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
