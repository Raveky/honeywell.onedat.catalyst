﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "select s1.STORAGE_LOCATION_CD,S1.LOCATION_NM FROM VALID_STORAGE_LOCATION s1 where ACTIVE_IND='Y' and STORAGE_TYPE_IND='C'";
            return _Query;
        }

        public string GetQueryBookNo(string bookNum)
        {           
            _Query = "select MAX(CAST(REPLACE(CATALYST_BOOK_CD, '" + bookNum + "', '') AS INTEGER)) as maxnum FROM CATALYST WHERE CATALYST_BOOK_CD LIKE '" + bookNum + "%'";
            return _Query;
        }
    }
}
