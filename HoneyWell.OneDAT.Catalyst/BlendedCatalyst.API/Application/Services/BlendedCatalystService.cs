﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using System.Dynamic;

namespace HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Application.Services
{
    public class BlendedCatalystService : OneDATService
    {
        Operations operations;
        Query customQuery;

        public BlendedCatalystService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            customQuery = new Query();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //validate logic goes here
            //throw new ValidationException("Validation Failed");

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Read(dataRequest);

            //incase of module specific read use custome query as under
            //Query qry = new Query();
            //var sqltext = qry.GetQuery();
            //return await operations.Read(dataRequest, sqltext);
        }
        public override async Task<OperationStatus> Read(DataRequest dataRequest,string query)
        {
            OperationStatus resultStatus = new OperationStatus();
            string catalyst_book_cd="";
            string precision;
            string bookNum = 'C' + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00"); ;
            query = customQuery.GetQueryBookNo(bookNum);
            OperationStatus bookNumTable = await operations.Read(dataRequest, query);
            ExpandoObject data = bookNumTable.DataResponse[0].Data[0];

            if (data.Where(v => v.Key == "maxnum").Select(x => x.Value).FirstOrDefault().ToString().Length<1)
            {
                catalyst_book_cd = bookNum + "01";
                
            }

            else
            {
                int num = Convert.ToInt32(data.Where(v => v.Key == "maxnum").Select(x => x.Value).FirstOrDefault().ToString());
                num += 1;
                if (num.ToString().Length == 1)
                    precision = '0' + num.ToString();
                else
                    precision = num.ToString();
                catalyst_book_cd = bookNum + precision;
            }

            //formation output data
            resultStatus.Status = "Success";
            var retObject = new List<dynamic>();
            retObject.Add(catalyst_book_cd);
            DataResponse dr = new DataResponse
            {
                TableName = "Catalyst",
                Data = retObject

            };
            resultStatus.DataResponse.Add(dr);

            return resultStatus;
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
