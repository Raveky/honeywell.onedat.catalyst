﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using System.Dynamic;

namespace BlendedCatalyst.API.Application.Services
{
    public class LocationService:OneDATService
    {
        Operations operations;
        Query customQuery;

        public LocationService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            customQuery = new Query();
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest, string query)
        {
            OperationStatus resultStatus = new OperationStatus();
            query = customQuery.GetQuery();
            return await operations.Read(dataRequest,query);
        }
       
    }
}
