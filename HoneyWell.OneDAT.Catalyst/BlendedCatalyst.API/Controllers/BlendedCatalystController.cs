﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.BlendedCatalyst.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BlendedCatalystController : ControllerBase
    {
        private readonly ILogger<BlendedCatalystController> _logger;

        private readonly IConfiguration _config;

        public BlendedCatalystController(ILogger<BlendedCatalystController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            BlendedCatalystService _rrs = new BlendedCatalystService(_config);
            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
        [HttpPost]
        [Route("/BookNo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncBook([FromBody] DataRequest dataRequest)
        {
            BlendedCatalystService _rrs = new BlendedCatalystService(_config);
            string query = "";
            OperationStatus operationstatus = await _rrs.Read(dataRequest, query);

            return Ok(operationstatus);
        }
        [HttpPost]
        [Route("/Location")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncLocation([FromBody] DataRequest dataRequest)
        {
            BlendedCatalystService _rrs = new BlendedCatalystService(_config);
            string query = "";
            OperationStatus operationstatus = await _rrs.Read(dataRequest, query);

            return Ok(operationstatus);
        }
    }
}
