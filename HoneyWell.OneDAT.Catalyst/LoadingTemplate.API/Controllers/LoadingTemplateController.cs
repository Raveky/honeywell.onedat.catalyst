﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoadingTemplateController : ControllerBase
    {
        private readonly ILogger<LoadingTemplateController> _logger;

        private readonly IConfiguration _config;

        public LoadingTemplateController(ILogger<LoadingTemplateController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            LoadingTemplateService _rrs = new LoadingTemplateService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }

        //Used to get Template information
        [HttpPost]
        [Route("/Template")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncTemplate([FromBody] DataRequest dataRequest)
        {
            LoadingTemplateService _rrs = new LoadingTemplateService(_config);
            var sql = "";
            OperationStatus operationstatus = await _rrs.Read(dataRequest, sql);

            return Ok(operationstatus);
        }
    }
}
