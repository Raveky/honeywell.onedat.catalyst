﻿using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoadingTemplate.API.Application.Model;

namespace HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        // Query to get max catalyst_loading_template_id_sq and cat_load_templ_reactor_assoc_id_sq values
        public string GetQuery()
        {
            _Query = "SELECT X.catalyst_loading_template_id_sq,Y.cat_load_templ_reactor_assoc_id_sq  FROM (SELECT Max(catalyst_loading_template_id_sq) catalyst_loading_template_id_sq FROM [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER]) X, (SELECT Max(cat_load_templ_reactor_assoc_id_sq) cat_load_templ_reactor_assoc_id_sq FROM [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC]) Y";
            return _Query;
        }

        //Query to get Bed details
        public string GetBedQuery(DataRequest dataRequest)
        {
            // This is Get Sp for Reactor Bed Details [catalyst].[Get_Reactor_Bed_Details_Sp] 

            int catalystLoadingTemplateId = 0;

            if (dataRequest.Messages[0].filter != null && dataRequest.Messages[0].filter.Count > 0)
            {
                foreach (var ele in dataRequest.Messages[0].filter)
                {
                    switch (ele.field_name.ToLower())
                    {
                        case "catalyst_loading_template_id":
                            if (ele.field_value.Length > 0)
                                catalystLoadingTemplateId = Convert.ToInt32(ele.field_value);
                            break;
                        default: break;
                    }
                }
            }

            _Query = "select t1.CATALYST_LOADING_TEMPLATE_ID_SQ TEMPLATE_ID_SQ,t1.BED_NUM,t1.CATALYST_VOLUME_MSR,t1.SPLIT_NM,t2.DILUENT_VOLUME_MSR,t1.DILUENT_ID,t1.DILUENT_NM from" +
                     "(select CATALYST_LOADING_TEMPLATE_ID_SQ, BED_NUM, SPLIT, PRMY_MATL_VOL_MSR CATALYST_VOLUME_MSR, DILUENT_ID AS DILUENT_ID, B.DILUENT_NM AS DILUENT_NM, SPLIT AS SPLIT_NM from [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] A join [sc_Catalyst].[DILUENT] B ON A.DILUENT_ID = B.DILUENT_ID_SQ where CATALYST_LOADING_TEMPLATE_ID_SQ = " + catalystLoadingTemplateId + " and PRMY_MATL_NM = 'Catalyst') as t1 " +
                     " join (select CATALYST_LOADING_TEMPLATE_ID_SQ, BED_NUM, PRMY_MATL_VOL_MSR DILUENT_VOLUME_MSR from[sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] where CATALYST_LOADING_TEMPLATE_ID_SQ = " + catalystLoadingTemplateId + " AND PRMY_MATL_NM = 'Diluent') as t2" +
                     " on t1.BED_NUM = t2.BED_NUM";
            return _Query;
        }

        public string GetTemplateQuery(DataRequest dataRequest)
        {
            string templateName = null;
            string status = null;
            string InternalDiaMsr = "null";
            string bedLengthMsr = "null";
            string bedVolumeMsr = "null";


            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var ele in dataRequest.Messages[0].filter)
                    {
                        switch (ele.field_name.ToLower())
                        {
                            case "catalyst_loading_template_nm":
                                templateName = ele.field_value;
                                break;
                            case "catalyst_status_cd":
                                status = ele.field_value;
                                break;
                            case "internal_diameter_msr":
                                if (ele.field_value.Length > 0)
                                    InternalDiaMsr = ele.field_value;
                                break;
                            case "bed_length_msr":
                                if (ele.field_value.Length > 0)
                                    bedLengthMsr = ele.field_value;
                                break;
                            case "bed_volume_msr":
                                if (ele.field_value.Length > 0)
                                    bedVolumeMsr = ele.field_value;
                                break;
                            default: break;
                        }
                    }
                }
            }
            //ISNULL(CAST(fpt.BED_LENGTH_MSR AS VARCHAR(30)), '') AS BED_LENGTH_MSR,
            _Query = "SELECT" +
                     " fpt.CATALYST_LOADING_TEMPLATE_ID_SQ," +
                     " fpt.CATALYST_LOADING_TEMPLATE_NM," +
                     " fpt.CAT_LOAD_TEMPL_REACTOR_ASSOC_ID_SQ," +
                     " ISNULL(fpt.BED_LENGTH_MSR,0) AS BED_LENGTH_MSR," +
                     " ISNULL(fpt.BED_VOLUME_MSR,0) AS BED_VOLUME_MSR," +
                     " fpt.STATUS_CD," +
                     " ISNULL(fpt.INTERNAL_DIAMETER_MSR,0) AS INTERNAL_DIAMETER_MSR," +
                      " (select STATUS_NM from [sc_Catalyst].[STATUS] WHERE STATUS_CD = fpt.STATUS_CD) STATUS_NM ," +
                     " fpt.LOAD_TYPE AS LOAD_TYPE" +
                     " FROM(" +
                         " SELECT" +
                           " spt.CATALYST_LOADING_TEMPLATE_ID_SQ," +
                           " spt.CATALYST_LOADING_TEMPLATE_NM," +
                           " spt.CAT_LOAD_TEMPL_REACTOR_ASSOC_ID_SQ," +
                           " spt.CATALYST_STATUS_CD STATUS_CD," +
                           " spt.BED_LENGTH_MSR," +
                           " spt.BED_VOLUME_MSR," +
                           " spt.INTERNAL_DIAMETER_MSR," +
                           " spt.LOAD_TYPE" +
                        " FROM(" +
                        " SELECT A.CATALYST_LOADING_TEMPLATE_ID_SQ," +
                           " A.CATALYST_LOADING_TEMPLATE_NM," +
                           " A.CATALYST_STATUS_CD," +
                           " B.CAT_LOAD_TEMPL_REACTOR_ASSOC_ID_SQ," +
                           " B.BED_LENGTH_MSR," +
                           " B.BED_VOLUME_MSR," +
                           " B.INTERNAL_DIAMETER_MSR," +
                           " STUFF(" +
                             " (SELECT '|' + FORMAT(CAST(PRMY_MATL_VOL_MSR AS decimal(30,1)),'#.0')[text()]" +
                             " FROM [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] t1" +
                             " WHERE t1.CATALYST_LOADING_TEMPLATE_ID_SQ = A.CATALYST_LOADING_TEMPLATE_ID_SQ AND PRMY_MATL_NM='CATALYST'" +
                              " ORDER BY BED_NUM" +
                              " FOR XML PATH(''))" +
                             ", 1, 1, '') AS LOAD_TYPE" +
                        " FROM [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] A" +
                        " LEFT JOIN [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC] B ON A.CATALYST_LOADING_TEMPLATE_ID_SQ = B.CATALYST_LOADING_TEMPLATE_ID_SQ" +
                            " WHERE(LOWER(A.CATALYST_LOADING_TEMPLATE_NM) LIKE '%' + LOWER(REPLACE(REPLACE('" + templateName + "', '_', CHAR(127) + '_'), '%', CHAR(127) + '%') + '%')" +
                            " ESCAPE CHAR(127) OR '" + templateName + "' IS NULL)" +
                            " AND(LOWER(A.CATALYST_STATUS_CD) LIKE '%' + LOWER(REPLACE(REPLACE('" + status + "', '_',CHAR(127) + '_'), '%', CHAR(127) + '%') + '%')" +
                            "  ESCAPE CHAR(127) OR '" + status + "' IS NULL)" +
                            " AND(ROUND(B.INTERNAL_DIAMETER_MSR, 6) = ROUND(" + InternalDiaMsr + ", 6) OR " + InternalDiaMsr + " IS NULL)" +
                            " AND(ROUND(B.BED_LENGTH_MSR, 6) = ROUND(" + bedLengthMsr + ", 6) OR  " + bedLengthMsr + " IS NULL)" +
                            " AND(ROUND(B.BED_VOLUME_MSR, 6) = ROUND(" + bedVolumeMsr + ", 6) OR  " + bedVolumeMsr + " IS NULL)" +
                         " ) spt" +
                     " ) fpt";
            //CAST(PRMY_MATL_VOL_MSR AS VARCHAR(30))[text()]
            return _Query;
        }

        //Query to add a Template
        public string GetAddQuery(DataRequest dataRequest, int templateIdSq, int reactorId)
        {
            List<BedModel> bedList;
            string catalystLoadingTemplateNm = null;
            string catalystStatusCd = null;
            decimal? internalDiameterMsr = null;
            decimal? bedLengthMsr = null;
            decimal? bedVolumeMsr = null;
            string createdByUserId = null;
            if (dataRequest.Messages != null)
            {
                if (dataRequest.Messages.Count > 0)
                {
                    foreach (var elements in dataRequest.Messages)
                    {
                        BedModel bed = new BedModel();
                        foreach (var element in elements.fields)
                        {
                            switch (element.field_name.ToLower())
                            {
                                case "catalyst_loading_template_nm":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        catalystLoadingTemplateNm = element.field_value;
                                    break;
                                case "catalyst_status_cd":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        catalystStatusCd = element.field_value;
                                    break;
                                case "internal_diameter_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        internalDiameterMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "bed_length_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        bedLengthMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "bed_volume_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        bedVolumeMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "created_by_user_id":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        createdByUserId = element.field_value;
                                    break;
                                default: break;
                            }
                        }
                    }
                }
            }
            Query query = new Query();
            bedList = query.GetBedModelList(dataRequest);
            _Query = "BEGIN TRANSACTION ";

            // Query which will insert into [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] table
            _Query += " INSERT INTO [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER](catalyst_loading_template_id_sq,catalyst_loading_template_nm,catalyst_status_cd,created_by_user_id,created_on_dt) VALUES (" + templateIdSq + ",'" + catalystLoadingTemplateNm + "','" + catalystStatusCd + "','" + createdByUserId + "',CONVERT(datetime,getdate(),105));";

            // Query which will insert into [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC]
            string bedLengthMsrS = (string.IsNullOrEmpty(bedLengthMsr.ToString())) ? "NULL" : bedLengthMsr.ToString();
            string bedVolumeMsrS = (string.IsNullOrEmpty(bedVolumeMsr.ToString())) ? "NULL" : bedVolumeMsr.ToString();
            string internalDiameterMsrS = (string.IsNullOrEmpty(internalDiameterMsr.ToString())) ? "NULL" : internalDiameterMsr.ToString();
            _Query += " INSERT INTO [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC](CAT_LOAD_TEMPL_REACTOR_ASSOC_ID_SQ,CATALYST_LOADING_TEMPLATE_ID_SQ,INTERNAL_DIAMETER_MSR,BED_LENGTH_MSR,BED_VOLUME_MSR,CREATED_BY_USER_ID,CREATED_ON_DT) VALUES (" + reactorId + ", " + templateIdSq + "," + internalDiameterMsrS + "," + bedLengthMsrS + "," + bedVolumeMsrS + ",'" + createdByUserId + "',CONVERT(datetime,getdate(),105));";

            // Query which will insert into [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC]
            _Query += " INSERT INTO [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC](CATALYST_LOADING_TEMPLATE_ID_SQ,BED_NUM,PRMY_MATL_NM,PRMY_MATL_VOL_MSR,SPLIT,DILUENT_ID,CREATED_BY_USER_ID,CREATED_ON_DT) VALUES";
            for (int i = 0; i < bedList.Count; i++)
            {
                // Adding CATALYST volume
                _Query += "(";
                _Query += "" + templateIdSq + "," + bedList[i].BedNum + ", 'Catalyst', " + bedList[i].CatalystVolumeMsr + "," + bedList[i].Split + "," + bedList[i].DiluentId + ",'" + createdByUserId + "',CONVERT(datetime,getdate(),105)),";
                //Adding DILUENT volume
                _Query += "(";
                _Query += "" + templateIdSq + "," + bedList[i].BedNum + ",'Diluent', " + bedList[i].DiluentVolumeMsr + "," + bedList[i].Split + "," + bedList[i].DiluentId + ",'" + createdByUserId + "',CONVERT(datetime,getdate(),105)";
                if (i < bedList.Count - 1)
                    _Query += "),";
                else
                    _Query += ");";
            }
            _Query += " COMMIT TRANSACTION";
            return _Query;
        }

        // Query to Update update a specific Template
        public string GetUpdateQuery(DataRequest dataRequest)
        {
            List<BedModel> bedList;
            int? catalystLoadingTemplateId = null;
            string catalystLoadingTemplateNm = null;
            string catalystStatusCd = null;
            decimal? internalDiameterMsr = null;
            decimal? bedLengthMsr = null;
            decimal? bedVolumeMsr = null;
            string updatedByUserId = null;
            if (dataRequest.Messages != null)
            {
                if (dataRequest.Messages.Count > 0)
                {
                    foreach (var elements in dataRequest.Messages)
                    {
                        BedModel bed = new BedModel();
                        foreach (var element in elements.fields)
                        {
                            switch (element.field_name.ToLower())
                            {
                                case "catalyst_loading_template_nm":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        catalystLoadingTemplateNm = element.field_value;
                                    break;
                                case "catalyst_status_cd":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        catalystStatusCd = element.field_value;
                                    break;
                                case "internal_diameter_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        internalDiameterMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "bed_length_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        bedLengthMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "bed_volume_msr":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        bedVolumeMsr = Convert.ToDecimal(element.field_value);
                                    break;
                                case "updated_by_user_id":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        updatedByUserId = element.field_value;
                                    break;
                                default: break;
                            }
                        }
                        foreach (var element in elements.filter)
                        {
                            switch (element.field_name.ToLower())
                            {
                                case "catalyst_loading_template_id_sq":
                                    if (!string.IsNullOrEmpty(element.field_value))
                                        catalystLoadingTemplateId = Convert.ToInt32(element.field_value);
                                    break;
                            }
                        }
                    }
                }
            }
            Query query = new Query();
            bedList = query.GetBedModelList(dataRequest);

            _Query = "BEGIN TRANSACTION ";

            // Query which will insert into [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] table
            _Query += " UPDATE [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] SET catalyst_loading_template_nm ='" + catalystLoadingTemplateNm + "',catalyst_status_cd='" + catalystStatusCd + "',updated_by_user_id='" + updatedByUserId + "',updated_on_dt = CONVERT(datetime,getdate(),105) where catalyst_loading_template_id_sq= " + catalystLoadingTemplateId + ";";

            // Query which will insert into [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC]
            string bedLengthMsrS = (string.IsNullOrEmpty(bedLengthMsr.ToString())) ? "NULL" : bedLengthMsr.ToString();
            string bedVolumeMsrS = (string.IsNullOrEmpty(bedVolumeMsr.ToString())) ? "NULL" : bedVolumeMsr.ToString();
            string internalDiameterMsrS = (string.IsNullOrEmpty(internalDiameterMsr.ToString())) ? "NULL" : internalDiameterMsr.ToString();
            _Query += " UPDATE [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC] SET INTERNAL_DIAMETER_MSR = " + internalDiameterMsrS + ",BED_LENGTH_MSR = " + bedLengthMsrS + ",BED_VOLUME_MSR =" + bedVolumeMsrS + ",UPDATED_BY_USER_ID='" + updatedByUserId + "',UPDATED_ON_DT=CONVERT(datetime,getdate(),105) WHERE CATALYST_LOADING_TEMPLATE_ID_SQ=" + catalystLoadingTemplateId + ";";

            // Deleting from [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC]
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] WHERE CATALYST_LOADING_TEMPLATE_ID_SQ=" + catalystLoadingTemplateId + ";";

            _Query += " INSERT INTO [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC](CATALYST_LOADING_TEMPLATE_ID_SQ,BED_NUM,PRMY_MATL_NM,PRMY_MATL_VOL_MSR,SPLIT,DILUENT_ID,CREATED_BY_USER_ID,CREATED_ON_DT) VALUES";
            for (int i = 0; i < bedList.Count; i++)
            {
                // Adding CATALYST volume data
                _Query += "(";
                _Query += "" + catalystLoadingTemplateId + "," + bedList[i].BedNum + ", 'Catalyst', " + bedList[i].CatalystVolumeMsr + "," + bedList[i].Split + "," + bedList[i].DiluentId + ",'" + updatedByUserId + "',CONVERT(datetime,getdate(),105)),";
                //Adding DILUENT volume data
                _Query += "(";
                _Query += "" + catalystLoadingTemplateId + "," + bedList[i].BedNum + ",'Diluent', " + bedList[i].DiluentVolumeMsr + "," + bedList[i].Split + "," + bedList[i].DiluentId + ",'" + updatedByUserId + "',CONVERT(datetime,getdate(),105)";
                if (i < bedList.Count - 1)
                    _Query += "),";
                else
                    _Query += ");";
            }
            _Query += " COMMIT TRANSACTION";
            return _Query;
        }

        // Query to Delete specific Template
        public string GetDeleteQuery(DataRequest dataRequest)
        {
            int? catalystLoadingTemplateId = null;
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        switch (data.field_name.ToLower())
                        {
                            case "catalyst_loading_template_id_sq":
                                catalystLoadingTemplateId = Convert.ToInt32(data.field_value);
                                break;
                            default: break;
                        }
                    }
                }
            }
            _Query = "BEGIN TRANSACTION ";
            // Deleting records from [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] table
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_BED_ASSOC] WHERE CATALYST_LOADING_TEMPLATE_ID_SQ=" + catalystLoadingTemplateId + ";";

            // Deleting records from [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC] table
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST_LOADING_TEMPLATE_REACTOR_ASSOC] WHERE CATALYST_LOADING_TEMPLATE_ID_SQ=" + catalystLoadingTemplateId + ";";

            // Deleting records from [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] table
            _Query += " DELETE FROM [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] where catalyst_loading_template_id_sq=" + catalystLoadingTemplateId + ";";

            _Query += " COMMIT TRANSACTION";
            return _Query;
        }

        // Query to get Bed details 
        public List<BedModel> GetBedModelList(DataRequest dataRequest)
        {
            List<BedModel> bedModels = new List<BedModel>();
            bool isBedDataAvailable = false; //to handle different formats

            if (dataRequest.Messages != null)
            {
                if (dataRequest.Messages.Count > 0)
                {
                    foreach (var elements in dataRequest.Messages)
                    {
                        BedModel bed = new BedModel();
                        foreach (var element in elements.fields)
                        {
                            switch (element.field_name.ToLower())
                            {
                                case "bed_num":
                                    if (element.field_value.Length > 0)
                                    {
                                        bed.BedNum = Convert.ToInt32(element.field_value);
                                        isBedDataAvailable = true;
                                    }
                                    break;
                                case "catalyst_volume_msr":
                                    if (element.field_value.Length > 0)
                                    {
                                        bed.CatalystVolumeMsr = Convert.ToDecimal(element.field_value);
                                        isBedDataAvailable = true;
                                    }
                                    break;
                                case "split":
                                    if (element.field_value.Length > 0)
                                    {
                                        bed.Split = Convert.ToInt32(element.field_value);
                                        isBedDataAvailable = true;
                                    }
                                    break;
                                case "diluent_id":
                                    if (element.field_value.Length > 0)
                                    {
                                        bed.DiluentId = Convert.ToInt32(element.field_value);
                                        isBedDataAvailable = true;
                                    }
                                    break;
                                case "diluent_volume_msr":
                                    if (element.field_value.Length > 0)
                                    {
                                        bed.DiluentVolumeMsr = Convert.ToDecimal(element.field_value);
                                        isBedDataAvailable = true;
                                    }
                                    break;
                                default: break;
                            }
                        }
                        if (isBedDataAvailable)
                            bedModels.Add(bed);
                        isBedDataAvailable = false;  // to handle different formats
                    }
                }
            }
            return bedModels;
        }

        // Query to check for Duplicate Template Name
        public string GetQuery(string templateName)
        {
            _Query = "select catalyst_loading_template_id_sq from [sc_Catalyst].[CATALYST_LOADING_TMPLT_MASTER] where CATALYST_LOADING_TEMPLATE_NM ='" + templateName + "'";
            return _Query;
        }
    }
}
