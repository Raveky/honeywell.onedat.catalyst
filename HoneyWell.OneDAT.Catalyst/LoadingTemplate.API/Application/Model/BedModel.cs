﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoadingTemplate.API.Application.Model
{
    public class BedModel
    {
        public int? BedNum { get; set; }
        public decimal? CatalystVolumeMsr { get; set; }
        public int? Split { get; set; }
        public int? DiluentId { get; set; }
        public decimal? DiluentVolumeMsr { get; set; }
    }
}
