﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using LoadingTemplate.API.Application.Validators;
using LoadingTemplate.API.Application.Model;

namespace HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Application.Services
{
    public class LoadingTemplateService : OneDATService
    {
        Operations operations;
        Query query;
        BusinessValidator bussinessValidator;
        OperationStatus objResult;
        public LoadingTemplateService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            query = new Query();
            bussinessValidator = new BusinessValidator(configuration);
            objResult = new OperationStatus();
        }

        // Method to create a Template
        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            // Code written to check for duplicate Template Name
            var readResult = await bussinessValidator.ValidateDuplicateTemplate(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            int templateIdKey = 0;
            int reactorId = 0;
            var data = await operations.Read(null, query.GetQuery());
            if (data.DataResponse.Count > 0)
            {
                foreach (var item in data.DataResponse[0].Data)
                {
                    IDictionary<string, object> propertyValues = item;

                    foreach (var property in propertyValues.Keys)
                    {
                        switch (property)
                        {
                            case "catalyst_loading_template_id_sq":
                                templateIdKey = Convert.ToInt32(propertyValues[property]);
                                break;
                            case "cat_load_templ_reactor_assoc_id_sq":
                                reactorId = Convert.ToInt32(propertyValues[property]);
                                break;
                        }
                    }
                }
            }

            string sqlQuery = query.GetAddQuery(dataRequest, templateIdKey + 1, reactorId + 1);
            var success = await operations.ExecuteDbQuery(sqlQuery);
            if (success.Status == "Success")
                success.Message = "Record Inserted successfully";
            return success;
        }

        // Method to Read Bed Information
        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here
            var readResult = await bussinessValidator.Validate(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            var sqlQuery = query.GetBedQuery(dataRequest);
            dataRequest.Messages[0].filter = null;
            return await operations.Read(dataRequest, sqlQuery);
        }

        // Method to update template information including Bead and Reactor 
        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            OperationStatus success = new OperationStatus();
            // Code written to check for duplicate Template Name
            var readResult = await bussinessValidator.ValidateDuplicateTemplate(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            success = await operations.ExecuteDbQuery(query.GetUpdateQuery(dataRequest));
            if (success.Status == "Success")
                success.Message = "Record Updated successfully";

            return success;
        }

        // Method to Delete a Template
        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here
            var readResult = await bussinessValidator.ValidateDelete(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            var success = await operations.ExecuteDbQuery(query.GetDeleteQuery(dataRequest));
            if (success.Status == "Success")
                success.Message = "Record Deleted successfully";
            return success;
        }

        // Method to read Template details
        public override async Task<OperationStatus> Read(DataRequest dataRequest, string sql)
        {
            sql = query.GetTemplateQuery(dataRequest);
            dataRequest.Messages[0].filter = null;
            return await operations.Read(dataRequest, sql);
        }
    }

}
