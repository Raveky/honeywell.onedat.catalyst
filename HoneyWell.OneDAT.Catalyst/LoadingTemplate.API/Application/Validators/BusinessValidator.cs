﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using HoneyWell.OneDAT.Catalyst.LoadingTemplate.API.Application.Queries;

namespace LoadingTemplate.API.Application.Validators
{
    public class BusinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BusinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }

        // Method to Validate read operation of Bed
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            bool catalystLoadingTemplateId = false;
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        switch (data.field_name.ToLower())
                        {
                            case "catalyst_loading_template_id":
                                catalystLoadingTemplateId = true;
                                if (data.field_value.Length < 1 || string.IsNullOrEmpty(data.field_value))
                                    return (false, "catalyst_loading_template_id_sq value should not be null");
                                break;
                            default: break;
                        }
                    }

                }
            }
            if (!catalystLoadingTemplateId)
                return (false, "template_id_sq paramerter is not provided to Read Bed data");
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateDuplicateTemplate(DataRequest dataRequest)
        {
            Field field = new Field();
            Query query = new Query();
            List<int> bedNumList = new List<int>();
            int? catalystLoadingTemplateId = null;
            bool status = false;
            if (dataRequest.Messages != null)
            {
                if (dataRequest.Messages.Count > 0)
                {
                    foreach (var elements in dataRequest.Messages)
                    {
                        foreach (var element in elements.fields)
                        {
                            switch (element.field_name.ToLower())
                            {
                                case "catalyst_loading_template_nm":
                                    field = element;
                                    break;
                                case "bed_num":
                                    if (element.field_value.Length > 0)
                                        if (bedNumList.Contains(Convert.ToInt32(element.field_value)))
                                            return (false, "Bed_Num should not repeat");
                                        else
                                            bedNumList.Add(Convert.ToInt32(element.field_value));
                                    break;
                                default: break;
                            }
                        }
                        // to get catalyst_loading_template_id_sq value
                        if (dataRequest.OperationType == "PUT" || dataRequest.OperationType == "PUT")
                        {
                            foreach (var element in elements.filter)
                            {
                                switch (element.field_name.ToLower())
                                {
                                    case "catalyst_loading_template_id_sq":
                                        if (!string.IsNullOrEmpty(element.field_value))
                                            catalystLoadingTemplateId = Convert.ToInt32(element.field_value);
                                        break;
                                    default: break;
                                }
                            }
                        }

                    }

                }
                else
                    return (false, "Please provide input data");
            }
            else
                return (false, "Please provide input data");

            if (dataRequest.OperationType == "PUT" || dataRequest.OperationType == "put")
            {
                var data = await operations.Read(null, query.GetQuery(field.field_value));
                if (data.DataResponse.Count > 0)
                {
                    foreach (var item in data.DataResponse[0].Data)
                    {
                        IDictionary<string, object> propertyValues = item;

                        foreach (var property in propertyValues.Keys)
                        {
                            switch (property)
                            {
                                case "catalyst_loading_template_id_sq":
                                    if (!string.IsNullOrEmpty(propertyValues[property].ToString()))
                                        if (catalystLoadingTemplateId != Convert.ToInt32(propertyValues[property]))
                                            return (false, "catalyst_loading_template_nm is already exist, Please provide unique value");
                                    break;
                            }
                        }
                    }
                }
            }
            else
                status = await commonValidations.ChecksDuplicateRecord(field, dataRequest);
            if (status)
            {
                return (false, "catalyst_loading_template_nm is already exist, Please provide unique value");
            }

            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateDelete(DataRequest dataRequest)
        {
            bool catalystLoadingTemplateId = false;
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        switch (data.field_name.ToLower())
                        {
                            case "catalyst_loading_template_id_sq":
                                catalystLoadingTemplateId = true;
                                if (data.field_value.Length < 1 || string.IsNullOrEmpty(data.field_value))
                                    return (false, "catalyst_loading_template_id_sq value should not be null");
                                break;
                            default: break;
                        }
                    }

                }
            }
            if (!catalystLoadingTemplateId)
                return (false, "template_id_sq paramerter is not provided to delete data");
            return (true, "Valid Data");
        }


    }
}
