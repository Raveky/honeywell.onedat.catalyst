﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Validators;
using HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Queries;

namespace HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Services
{
    public class ManageCatalystStorageLocationService: OneDATService
    {
        Operations operations;
        BusinessValidator bussinessValidator;
        OperationStatus objResult;
        Query getSqlQuery;
        public ManageCatalystStorageLocationService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BusinessValidator(configuration);
            objResult = new OperationStatus();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //Creation operation is not there

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here
            Query getSqlQuery = new Query();
            string query = getSqlQuery.GetQuery();

            return await operations.Read(dataRequest, query);
        }
        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }
    }
}
