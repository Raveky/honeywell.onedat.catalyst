﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Validators
{
    public class BusinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BusinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            return (true, "Valid Data");
        }
        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
           
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateUpdate(DataRequest dataRequest)
        {          
            return (true, "Valid Data");
        }

    }
}
