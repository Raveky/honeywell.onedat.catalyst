﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Queries
{
    public class Query:IQuery
    {
        private string _Query;

        public string GetQuery()
        {

            _Query = "SELECT distinct A.CATALYST_LOAD_RQST_CD,--Load_Request_Assoc.CATALYST_ID" +
                     "CASE (" +
                     " SELECT COUNT(DISTINCT CATALYST_ID)" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC]" +
                     " WHERE CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     ")" +
                     " WHEN 1 THEN(" +
                     "SELECT TOP 1 CATALYST_ID" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC]" +
                     " WHERE CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     ")" +
                     " ELSE 0 END AS CATALYST_ID," +
                     " CASE(" +
                     "SELECT COUNT(DISTINCT CATALYST_ID)" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC]" +
                     " WHERE CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     ")" +
                     " WHEN 1 THEN(" +
                     "SELECT ISNULL(CATALYST_BOOK_CD, CATALYST_UOP_CD)" +
                     " FROM [sc_Catalyst].[CATALYST]" +
                     " WHERE CATALYST_ID_SQ = (" +
                     "SELECT TOP 1 CATALYST_ID" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC]" +
                     "  WHERE CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     ")" +
                     ")" +
                     " ELSE STUFF(" +
                     " (" +
                     "SELECT '/' + ISNULL(CATALYST_BOOK_CD, CATALYST_UOP_CD)" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC] BED" +
                     " JOIN [sc_Catalyst].[CATALYST] C ON BED.CATALYST_ID = C.CATALYST_ID_SQ AND BED.CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     " FOR XML PATH('')" +
                     ") ,1,1,''" +
                     ") END AS CATALYST_DESIGNATION," +
                     "  STUFF(" +
                     "(" +
                     " SELECT DISTINCT '/' + c.CATALYST_DESC" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC] BED" +
                     " JOIN [sc_Catalyst].[CATALYST] C ON BED.CATALYST_ID = C.CATALYST_ID_SQ AND BED.CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     " FOR XML PATH('')" +
                     ") ,1,1,''" +
                     " ) AS CATALYST_DESC," +
                     " STUFF(" +
                     " (" +
                     " SELECT '/' + CAST(ACTL_LOAD_VALUE_MSR * 1000000 AS VARCHAR(50))" +
                     " FROM [sc_Catalyst].[CATALYST_LOAD_BED_ASSOC] BED" +
                     " WHERE BED.CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD" +
                     " FOR XML PATH('')" +
                     " ) ,1,1,''" +
                     " ) +'cc' AS LOAD_TYPE," +
                     " A.STORAGE_LOCATION_CD, ACTL_STOR_VOL_MSR,A.LOAD_NUM,A.DSPLY_ORD_NUM,B.LOCATION_NM" +
                     " FROM dbo.Get_Unused_Ldg_From_StorgLoc_Fn() as purge " +
                     " INNER JOIN [sc_Catalyst].[CATALYST_LOAD_LOC_ASSOC] A on purge.CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD AND purge.STORAGE_LOCATION_CD = A.STORAGE_LOCATION_CD" +
                     " INNER JOIN [sc_Catalyst].[VALID_STORAGE_LOCATION] B ON A.STORAGE_LOCATION_CD=B.STORAGE_LOCATION_CD" +
                     " INNER JOIN [sc_Catalyst].[CATALYST_LOAD_AUDIT_DTL] CLAD ON CLAD.CATALYST_LOAD_RQST_CD = A.CATALYST_LOAD_RQST_CD AND CLAD.STORAGE_LOCATION_CD = A.STORAGE_LOCATION_CD  AND purge.LOAD_NUM = CLAD.LOAD_NUM" +
                     " WHERE (purge.CAT_LOAD_AUDIT_STATUS_CD IN ('CLPDISG', 'CLPRVSD') AND purge.PURGE_ACK_IND = 'N') OR purge.CAT_LOAD_AUDIT_STATUS_CD IN ('CLPNEW', 'CLPASC','CLPKEEP')" +
                     " ORDER BY A.STORAGE_LOCATION_CD,A.DSPLY_ORD_NUM";
            return _Query;
        }
    }
}
