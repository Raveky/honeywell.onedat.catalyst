﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace HoneyWell.OneDAT.ManageCatalystStorageLocation.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ManageCatalystStorageLocationController : ControllerBase
    {
        private readonly ILogger<ManageCatalystStorageLocationController> _logger;

        private readonly IConfiguration _config;

        public ManageCatalystStorageLocationController(ILogger<ManageCatalystStorageLocationController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            ManageCatalystStorageLocationService _rrs = new ManageCatalystStorageLocationService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}