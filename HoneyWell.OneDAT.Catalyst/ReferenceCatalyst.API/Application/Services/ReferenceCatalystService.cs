﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;
using HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Application.Validators;

namespace HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Application.Services
{
    public class ReferenceCatalystService : OneDATService
    {
        Operations operations;
        BussinessValidator insertValidator;
        OperationStatus objResult = new OperationStatus();
        public ReferenceCatalystService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            insertValidator = new BussinessValidator(configuration);
        }
        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await insertValidator.Validate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Create(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Read(dataRequest);

            //incase of module specific read use custome query as under
            //Query qry = new Query();
            //var sqltext = qry.GetQuery();
            //return await operations.Read(dataRequest, sqltext);
        }


        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await insertValidator.UpdateValidate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Update(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
