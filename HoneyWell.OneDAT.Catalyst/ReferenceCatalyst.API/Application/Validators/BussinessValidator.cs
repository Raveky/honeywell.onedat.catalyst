﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Application.Validators
{
    public class BussinessValidator : IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base()
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "refr_catalyst_nm")
                    {
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);
                        if (isDuplicateValue)
                        {                         
                            return (false, "REFERENCECATALYSTALREADYEXISTS");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
        public async Task<(bool status, string message)> UpdateValidate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    if (data.field_name == "refr_catalyst_nm")
                    {
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);
                        if (isDuplicateValue)
                        {
                            return (false, "REFERENCECATALYSTALREADYEXISTS");
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }
    }
}

