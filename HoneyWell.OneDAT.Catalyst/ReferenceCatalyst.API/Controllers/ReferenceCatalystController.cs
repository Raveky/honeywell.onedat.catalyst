﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace HoneyWell.OneDAT.Catalyst.ReferenceCatalyst.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ReferenceCatalystController : ControllerBase
    {
        private readonly ILogger<ReferenceCatalystController> _logger;

        private readonly IConfiguration _config;

        public ReferenceCatalystController(ILogger<ReferenceCatalystController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            ReferenceCatalystService _rrs = new ReferenceCatalystService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}