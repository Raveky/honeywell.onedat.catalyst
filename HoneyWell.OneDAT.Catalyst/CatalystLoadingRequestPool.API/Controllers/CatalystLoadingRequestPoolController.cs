﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.CatalystLoadingRequestPool.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.CatalystLoadingRequestPool.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalystLoadingRequestPoolController : ControllerBase
    {
        private readonly ILogger<CatalystLoadingRequestPoolController> _logger;

        private readonly IConfiguration _config;

        public CatalystLoadingRequestPoolController(ILogger<CatalystLoadingRequestPoolController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            CatalystLoadingRequestPoolService _rrs = new CatalystLoadingRequestPoolService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}
