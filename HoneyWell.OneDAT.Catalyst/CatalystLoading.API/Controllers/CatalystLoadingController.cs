﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.CatalystLoading.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.CatalystLoading.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalystLoadingController : ControllerBase
    {
        private readonly ILogger<CatalystLoadingController> _logger;

        private readonly IConfiguration _config;

        public CatalystLoadingController(ILogger<CatalystLoadingController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            CatalystLoadingService _rrs = new CatalystLoadingService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
    }
}
