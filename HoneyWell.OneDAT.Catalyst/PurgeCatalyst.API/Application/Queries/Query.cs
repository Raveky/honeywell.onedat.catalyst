﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.PurgeCatalyst.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {

            return _Query;
        }
    }
}
