﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Catalyst.PurgeCatalyst.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;

namespace HoneyWell.OneDAT.Catalyst.PurgeCatalyst.API.Application.Services
{
    public class PurgeCatalystService : OneDATService
    {
        Operations operations;

        public PurgeCatalystService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //validate logic goes here
            //throw new ValidationException("Validation Failed");

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Read(dataRequest);

            //incase of module specific read use custome query as under
            //Query qry = new Query();
            //var sqltext = qry.GetQuery();
            //return await operations.Read(dataRequest, sqltext);
        }


        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
