﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.CatalystLoadingConfiguration.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.CatalystLoadingConfiguration.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatalystLoadingConfigurationController : ControllerBase
    {
        private readonly ILogger<CatalystLoadingConfigurationController> _logger;

        private readonly IConfiguration _config;

        public CatalystLoadingConfigurationController(ILogger<CatalystLoadingConfigurationController> logger, IConfiguration config)
        {
            _logger = logger;

            _config = config;
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            ManageLoadingConfigurationService _rrs = new ManageLoadingConfigurationService(_config);

            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
        [HttpPost]
        [Route("/Plant")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncPlant([FromBody] DataRequest dataRequest)
        {
            ManageLoadingConfigurationService _rrs = new ManageLoadingConfigurationService(_config);
            var query = "";

            OperationStatus operationstatus = await _rrs.Read(dataRequest,query);

            return Ok(operationstatus);
        }

        
    }
}
