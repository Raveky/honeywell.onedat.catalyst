﻿using HoneyWell.OneDAT.Catalyst.CatalystLoadingConfiguration.API.Application.Exceptions;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.CatalystLoadingConfiguration.API.Application.Validators
{
    public class BusinessValidator : OneDATService, IValidator
    {
        Operations operations;
        OperationStatus operationStatus;
        CommonValidations commonValidations;

        public BusinessValidator(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            return (true, "Valid Data");
        }
        public async Task<(bool status, string message)> ValidateRead(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        if (data.field_name == "plant_cd")
                        {
                            if (data.field_value == "--Select--")
                            {
                                return (false, "Select Plant");
                            }
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "Blank Plant Value");
                            }

                        }
                    }

                }
            }
            return (true, "Valid Data");
        }

        public async Task<(bool status, string message)> ValidateUpdate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields != null)
            {
                if (dataRequest.Messages[0].fields.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].fields)
                    {
                        if (data.field_name == "DFLT_CATALYST_LOAD_MSR")
                        {
                            if (data.field_value.Length < 1)
                            {
                                return (false, "Max.Loading Size is null or empty");
                            }
                        }
                        if (data.field_name == "PLANT_CD")
                        {
                            if (data.field_value == "--Select--")
                            {
                                return (false, "select Plant");
                            }
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "PLANT_CD is null or empty");
                            }
                            else
                            {
                                bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);
                                if (!isDuplicateValue)
                                {
                                    return (false, "PLANT_CD is a primary key, Couldn't update it");
                                }
                            }
                            
                        }

                    }
                }
            }

            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {

                        if (data.field_name == "PLANT_CD")
                        {
                            if (data.field_value == "--Select--")
                            {
                                return (false, "Select Plant");
                            }
                            if (string.IsNullOrEmpty(data.field_value))
                            {
                                return (false, "Plant_cd is null or empty");
                            }
                            else
                            {
                                bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);
                                if (!isDuplicateValue)
                                {
                                    return (false, "Record is not found to update");
                                }
                            }
                        }
                        if (data.field_name == "DFLT_CATALYST_LOAD_MSR")
                        {
                            if (data.field_value.Length < 1)
                            {
                                return (false, "Max.Loading Size is null");
                            }
                        }
                    }
                }
            }
            return (true, "Valid Data");
        }

    }
}

