﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.CatalystLoadingConfiguration.API.Application.Validators;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using HoneyWell.OneDAT.Module.SubModule.API.Application.Queries;
using Microsoft.Extensions.Configuration;
using Microsoft.Rest.TransientFaultHandling;

namespace HoneyWell.OneDAT.Catalyst.CatalystLoadingConfiguration.API.Application.Services
{
    public class ManageLoadingConfigurationService : OneDATService
    {
        Operations operations;
        BusinessValidator bussinessValidator;
        OperationStatus objResult;
        Query customQuery;
        public ManageLoadingConfigurationService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BusinessValidator(configuration);
            objResult = new OperationStatus();
            customQuery = new Query();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            var readResult = await bussinessValidator.ValidateRead(dataRequest);
            string query = customQuery.GetQuerySearch(dataRequest);
            if (!readResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = readResult.message.ToString();
                return objResult;
            }
            dataRequest.Messages[0].filter = null;
            return await operations.Read(dataRequest, query);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest, string query)
        {
            query = customQuery.GetQuery();
            dataRequest.Messages[0].filter = null;
            return await operations.Read(dataRequest, query);
        }

        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            var updateResult = await bussinessValidator.ValidateUpdate(dataRequest);
            if (!updateResult.status)
            {
                objResult.Status = "Fail";
                objResult.Message = updateResult.message.ToString();
                return objResult;
            }
            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
