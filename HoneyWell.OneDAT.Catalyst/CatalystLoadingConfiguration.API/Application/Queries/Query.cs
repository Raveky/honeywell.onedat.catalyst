﻿
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Module.SubModule.API.Application.Queries
{
    public class Query : IQuery
    {
        private string _Query;

        public string GetQuery()
        {
            _Query = "select PLANT_CD, PLANT_DESC FROM [sc_Plant].[PLANT] WHERE ACTIVE_IND='Y'";
            return _Query;
        }
        public string GetQuerySearch(DataRequest dataRequest)
        {
            string plant_cd = null;
            float? max_loading = null;
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        if (data.field_name == "PLANT_CD")
                            if (!string.IsNullOrEmpty(data.field_value))
                                plant_cd = data.field_value;
                        if (data.field_name == "DFLT_CATALYST_LOAD_MSR")
                            if (!string.IsNullOrEmpty(data.field_value))
                                max_loading = float.Parse(data.field_value);
                    }
                }
            }
            // Constructing query with above filters
            _Query = "SELECT PLANT_CD, DFLT_CATALYST_LOAD_MSR " +
                      "FROM [sc_Plant].[PLANT] " +
                      "WHERE (LOWER(PLANT_CD) LIKE '%'+ LOWER(REPLACE(REPLACE('" + plant_cd + "', '_', CHAR(127) + '_'), '%' ,CHAR(127) + '%') + '%')" +
                      " ESCAPE CHAR(127) OR '" + plant_cd + "' IS NULL)" +
                      " AND (LOWER(DFLT_CATALYST_LOAD_MSR) LIKE '%' + LOWER(REPLACE(REPLACE('" + max_loading + "', '_', CHAR(127) + '_'), '%' , CHAR(127) + '%') + '%')" +
                      " ESCAPE CHAR(127) OR '" + max_loading + "' IS NULL)" +
                      " AND ACTIVE_IND='Y'";

            return _Query;

        }
    }
}
