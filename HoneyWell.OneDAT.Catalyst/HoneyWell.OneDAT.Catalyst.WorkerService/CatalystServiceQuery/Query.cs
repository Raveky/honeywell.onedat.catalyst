﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using HoneyWell.OneDAT.Catalyst.WorkerService.Validators;

namespace HoneyWell.OneDAT.Catalyst.WorkerService.CatalystServiceQuery
{
    public class Query : IQuery
    {
        private string _Query = string.Empty;
        public string GetQuery()
        {
            _Query = @"";

            return _Query;
        }

        public string GetQuery(string queryName)
        {
            switch (queryName.ToUpper())
            {              
                case "CATALYST_RUN_DETAILS":               
                    return _Query = @"SELECT CATALYST_BOOK_CD,CATALYST_UOP_CD,CATALYST_DESIGNATION_NM,CATALYST_FAMILY_NM,CATALYST_ABD_MSR,EST_ABD_MSR,CATALYST_DESC,CATALYST_ID_SQ,SRC_SYSTEM_ID FROM sc_Catalyst.DW_CATALYST;";
                default:
                    return _Query;
            }
        }
    }
}


