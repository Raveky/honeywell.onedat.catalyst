﻿using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Catalyst.WorkerService.CatalystServiceQuery;
using System.IO.Compression;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net;

namespace HoneyWell.OneDAT.Catalyst.WorkerService.Services
{
    public class CatalystDATService : OneDATService
    {
        Operations operations;
        OperationStatus objResult = new OperationStatus();

        public CatalystDATService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }

        public CatalystDATService(string connectionString) : base(connectionString)
        {
            operations = new Operations(connectionString);
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                return await operations.Create(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {
            try
            {
                Query objQuery = new Query();
                if (dataRequest != null)
                {
                    if (dataRequest.Messages != null && dataRequest.Messages.Count > 0)
                    {
                        string sqlQuery = objQuery.GetQuery(dataRequest.Messages[0].table_name);

                        if (sqlQuery == null)
                            return new OperationStatus { Status = "Fail", Message = "Query Name is either incorrect or null", DataResponse = new List<DataResponse>() };
                        else                       
                        return await operations.Read(null, sqlQuery);
                    }
                }
                return new OperationStatus { Status = "Fail", Message = "Request is null", DataResponse = new List<DataResponse>() };
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory,
                    CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }

        public static byte[] DeCompress(byte[] decompress, byte[] compressed, int length)
        {
            using (MemoryStream memory = new MemoryStream(compressed, 0, compressed.Length))
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Decompress, true))
                {
                    int dsize = gzip.Read(decompress, 0, length);
                }

                return memory.ToArray();
            }
        }
        public override async Task<OperationStatus> Read(DataRequest dataRequest, string sqlQuery)
        {
            try
            {
                Query objQuery = new Query();
                sqlQuery = objQuery.GetQuery();

                var queryResult = await operations.Read(dataRequest, sqlQuery);
                byte[] text = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(queryResult.DataResponse[0].Data));

                // Use compress method.
                byte[] compress = Compress(text);

                byte[] decompress = new byte[text.Length];

                DeCompress(decompress, compress, text.Length);

                List<dynamic> lst = JsonConvert.DeserializeObject<List<dynamic>>(Encoding.UTF8.GetString(decompress));

                return await operations.Read(dataRequest, sqlQuery);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }
        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            try
            {
                return await operations.Update(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            try
            {
                return await operations.Delete(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }
        }

    }
}
