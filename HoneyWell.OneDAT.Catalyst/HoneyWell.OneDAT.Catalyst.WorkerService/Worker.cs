using HoneyWell.OneDAT.Commons.Integration.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IMessageService _worker;

        public Worker(ILogger<Worker> logger, IMessageService worker)
        {
            _logger = logger;
            _worker = worker;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _worker.DoWork(stoppingToken, _logger);
        }
    }
}
