﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Diluent.API.Application.Services;
using HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Services;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.ApplicationInsights.WindowsServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HoneyWell.OneDAT.Catalyst.Diluent.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DiluentController : ControllerBase
    {
        private readonly ILogger<DiluentController> _logger;

        private readonly IConfiguration _config;

        public DiluentController(ILogger<DiluentController> logger , IConfiguration config)
        {
            _logger = logger;

            _config = config;             
        }

        [HttpPost]
        [Route("/")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsync([FromBody] DataRequest dataRequest)
        {
            DiluentService _rrs = new DiluentService(_config);
            OperationStatus operationstatus = await _rrs.ProcessRequest(dataRequest);

            return Ok(operationstatus);
        }
        [HttpPost]
        [Route("/size")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<OperationStatus>> PostAsyncSize([FromBody] DataRequest dataRequest)
        {
            DiluentSizeService _rrs = new DiluentSizeService(_config);

            string sqlQuery = "";
            OperationStatus operationstatus = await _rrs.Read(dataRequest,sqlQuery);

            return Ok(operationstatus);
        }

    }
}
