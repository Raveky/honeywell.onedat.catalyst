﻿using HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Queries;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diluent.API.Application.Services
{
    public class DiluentSizeService : OneDATService
    {
        Operations operations;

        public DiluentSizeService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
        }
        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            //validate logic goes here
            //throw new ValidationException("Validation Failed");

            return await operations.Create(dataRequest);
        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest,string sqlQuery)
        {
             Query qry = new Query();
             sqlQuery = qry.GetQuery();
             return await operations.Read(dataRequest, sqlQuery);
        }


        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }
}
