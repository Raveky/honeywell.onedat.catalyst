﻿using HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Queries;
using HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Validators;
using HoneyWell.OneDAT.Commons;
using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Services
{
    public class DiluentService : OneDATService
    {
        Operations operations;
        BussinessValidator bussinessValidator;
        OperationStatus objResult;
        public DiluentService(IConfiguration configuration) : base(configuration)
        {
            operations = new Operations(configuration);
            bussinessValidator = new BussinessValidator(configuration);
            objResult = new OperationStatus();
        }

        public override async Task<OperationStatus> Create(DataRequest dataRequest)
        {
            try
            {
                var insertResult = await bussinessValidator.Validate(dataRequest);

                if (!insertResult.status)
                {
                    objResult.Status = "Fail";
                    objResult.Message = insertResult.message.ToString();
                    return objResult;
                }

                return await operations.Create(dataRequest);
            }
            catch (Exception ex)
            {
                objResult.Status = "Fail";
                objResult.Message = "Error :" + ex.ToString();
                return objResult;
            }

        }

        public override async Task<OperationStatus> Read(DataRequest dataRequest)
        {

            Query qry = new Query();
            string sqlQuery = qry.getquerystring(dataRequest);
            dataRequest.Messages[0].filter = null;
            return await operations.Read(dataRequest, sqlQuery);
        }


        public override async Task<OperationStatus> Update(DataRequest dataRequest)
        {

            return await operations.Update(dataRequest);
        }

        public override async Task<OperationStatus> Delete(DataRequest dataRequest)
        {
            //validate logic goes here

            return await operations.Delete(dataRequest);
        }

    }

}
