﻿using HoneyWell.OneDAT.Commons.CRUD;
using HoneyWell.OneDAT.Commons.Infrastructure.Validations;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Validators
{
    public class BussinessValidator : IValidator
    {
        readonly Operations operations;
        readonly OperationStatus operationStatus;
        readonly CommonValidations commonValidations;

        public BussinessValidator(IConfiguration configuration) : base()
        {
            operations = new Operations(configuration);
            operationStatus = new OperationStatus();
            commonValidations = new CommonValidations(configuration);
        }

      
        public async Task<(bool status, string message)> Validate(DataRequest dataRequest)
        {
            if (dataRequest.Messages[0].fields.Count > 0)
            {
                foreach (var data in dataRequest.Messages[0].fields)
                {
                    
                    if (data.field_name.ToLower() == "diluent_nm")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Enter Diluent Designation");
                        }
                       
                        bool isDuplicateValue = await commonValidations.ChecksDuplicateRecord(data, dataRequest);
                        if (isDuplicateValue)
                        {
                            return (false, "Duplicate Name Present");
                        }
                    }

                    if (data.field_name.ToLower() == "diluent_pd_vf")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Piece Density");
                        }
                        if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]*\.?[0-9]+$")))
                        {
                            return (false, "Invalid Piece Density");
                        }

                    }
                    if (data.field_name.ToLower() == "diluent_abd_vf")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank Apparent Bulk Density");
                        }
                        if ((!System.Text.RegularExpressions.Regex.IsMatch(data.field_value, @"^[0-9]*\.?[0-9]+$")))
                        {
                            return (false, "Invalid Apparent Bulk Density");
                        }

                    }
                    if (data.field_name.ToLower() == "active_ind")
                    {
                        if (string.IsNullOrEmpty(data.field_value))
                        {
                            return (false, "Blank diluent Status");
                        }
                        else if (!(data.field_value.ToLower() == "y" || data.field_value.ToLower() == "n"))
                        {
                            return (false, "Invalid value of Status");
                        }
                    }
                }
            }
                return (true, "Valid Data");
            }
       
        }
    }


