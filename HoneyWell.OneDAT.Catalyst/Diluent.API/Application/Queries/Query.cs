﻿using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.Commons.Model;

namespace HoneyWell.OneDAT.Catalyst.Diluent.API.Application.Queries
{
    public class Query : IQuery
    {

        private string _Query;
        public string GetQuery()
        {
            _Query = "SELECT 0 AS DILUENT_SIZE_ID_SQ , 'Select' AS DILUENT_SIZE_NM  UNION  SELECT DILUENT_SIZE_ID_SQ AS DILUENT_SIZE_ID_SQ,DILUENT_SIZE_NM AS DILUENT_SIZE_NM " +
                "FROM sc_Catalyst.CATALYST_DILUENT_SIZE ORDER BY DILUENT_SIZE_ID_SQ";
            return _Query;
        }
        public string getquerystring(DataRequest dataRequest)
        {
            string DiluentName = null;
            string DiluentDescriptionName = null;
            string DiluentSourceName = null;
            string DiluentPdVf = "null";
            string DiluentAbdVf = "null";
            string ActiveInd = null;
            string RefernceInd = null;
            string SizeId = "null";
            if (dataRequest.Messages[0].filter != null)
            {
                if (dataRequest.Messages[0].filter.Count > 0)
                {
                    foreach (var data in dataRequest.Messages[0].filter)
                    {
                        if (data.field_name.ToUpper() == "DILUENT_NM")
                            DiluentName = data.field_value;
                        if (data.field_name.ToUpper() == "DILUENT_DESC")
                            DiluentDescriptionName = data.field_value;
                        if (data.field_name.ToUpper() == "DILUENT_SOURCE_NM")
                            DiluentSourceName = data.field_value;
                        if (data.field_name.ToUpper() == "DILUENT_PD_VF")
                            DiluentPdVf = data.field_value;
                        if (data.field_name.ToUpper() == "DILUENT_ABD_VF")
                            DiluentAbdVf = data.field_value;
                        if (data.field_name.ToUpper() == "active_ind")
                            ActiveInd = data.field_value;
                        if (data.field_name.ToUpper() == "REFERENCE_DILUENT_IND")
                            RefernceInd = data.field_value;
                        if (data.field_name.ToUpper() == "diluent_size_id_sq")
                            SizeId = data.field_value;
                    }
                }
            }
            _Query = "SELECT fpt.DILUENT_ID_SQ,fpt.DILUENT_NM,fpt.DILUENT_DESC,fpt.DILUENT_SOURCE_NM,fpt.DILUENT_SIZE_ID,fpt.DILUENT_SIZE_NM,fpt.DILUENT_PD_VF,fpt.DILUENT_ABD_VF,fpt.REFERENCE_DILUENT_IND,fpt.ACTIVE_IND " +
                "FROM( SELECT spt.DILUENT_ID_SQ, spt.DILUENT_NM,spt.DILUENT_DESC,spt.DILUENT_SOURCE_NM,spt.DILUENT_SIZE_ID,spt.DILUENT_SIZE_NM,spt.DILUENT_PD_VF,spt.DILUENT_ABD_VF,spt.REFERENCE_DILUENT_IND,spt.ACTIVE_IND " +
                "FROM ( SELECT A.DILUENT_ID_SQ,A.DILUENT_NM,  A.DILUENT_DESC, A.DILUENT_SOURCE_NM,a.DILUENT_SIZE_ID,b.DILUENT_SIZE_NM DILUENT_SIZE_NM, A.DILUENT_PD_VF, A.DILUENT_ABD_VF, A.REFERENCE_DILUENT_IND,A.ACTIVE_IND " +
                "FROM sc_Catalyst.DILUENT A  JOIN sc_Catalyst.CATALYST_DILUENT_SIZE B ON A.DILUENT_SIZE_ID = B.DILUENT_SIZE_ID_SQ " +
                "WHERE (LOWER(A.DILUENT_NM) LIKE '%'+ LOWER(REPLACE(REPLACE( '" + DiluentName + "','_',CHAR(127)  + '_'), '%',CHAR(127)  + '%')  +  '%')  ESCAPE CHAR(127) OR '" + DiluentName + "' IS NULL) " +
                "AND (LOWER(A.DILUENT_DESC) LIKE '%'+ LOWER(REPLACE(REPLACE( '" + DiluentDescriptionName + "','_',CHAR(127)  + '_'), '%',CHAR(127)  + '%')  +  '%') ESCAPE CHAR(127) OR '" + DiluentDescriptionName + "' IS NULL) " +
                "AND (LOWER(A.DILUENT_SOURCE_NM) LIKE '%'+ LOWER(REPLACE(REPLACE( '" + DiluentSourceName + "','_',CHAR(127)  + '_'), '%',CHAR(127)  + '%')  +  '%') ESCAPE CHAR(127) OR '" + DiluentSourceName + "' IS NULL) " +
                "AND (ROUND(A.DILUENT_PD_VF,6) =ROUND(" + DiluentPdVf + ", 6) OR " + DiluentPdVf + "  IS NULL) " +
                "AND (ROUND(A.DILUENT_ABD_VF,6) =ROUND(" + DiluentAbdVf + ",6) OR  " + DiluentAbdVf + " IS NULL) " +
                "AND (LOWER(A.ACTIVE_IND) LIKE '%'+ LOWER(REPLACE(REPLACE( '" + ActiveInd + "','_',CHAR(127)  + '_'), '%',CHAR(127)  + '%')  +  '%')ESCAPE CHAR(127) OR  '" + ActiveInd + "' IS NULL) " +
                "AND (LOWER(A.REFERENCE_DILUENT_IND) LIKE '%'+ LOWER(REPLACE(REPLACE( '" + RefernceInd + "','_',CHAR(127)  + '_'), '%',CHAR(127)  + '%')  +  '%') ESCAPE CHAR(127) OR  '" + RefernceInd + "' IS NULL) " +
                "AND  (A.DILUENT_SIZE_ID = " + SizeId + " OR " + SizeId + " IS NULL) " +
               " )spt " +
               ")fpt ";
            return _Query;
        }
    }
}
